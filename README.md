# Getting Started with 2048

This game (2048) was built using **React** and **TypeScript**. The unique part of this example is animations. The animations in React aren't that straightforward, so I hope you can learn something new from it.

**Wondering how was that built?** You can find a video tutorial on [ YouTube Channel](https://www.youtube.com/channel/UCJV16_5c4A0amyBZSI4yP6A)

## How To Play?

Have to run below command after publishing docker image to docker registry

docker run -p 8081:3000 richeb/2048-game


